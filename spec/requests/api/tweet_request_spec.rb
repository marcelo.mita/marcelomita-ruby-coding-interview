require 'rails_helper'

RSpec.describe 'API Tweets', type: :request do
  describe '#create' do
    let(:response_body) { JSON.parse(response.body) }

    context 'with valid parameters' do
      let(:user1) { create(:user) }
      let(:valid_body) { 'This is a valid tweet' }

      it 'returns a successful response' do
        post api_tweets_path(user_id: user1.id, body: valid_body)

        expect(response).to have_http_status(:success)
      end

      it 'creates a new tweet' do
        expect do
          post api_tweets_path(user_id: user1.id, body: valid_body)
        end.to change(Tweet, :count).by(1)
      end
    end

    context 'With invalid parameters' do
      let(:user1) { create(:user) }

      context 'When the body is too long' do
        let(:invalid_body) { 'a' * 181 }

        it 'returns an error response' do
          post api_tweets_path(user_id: user1.id, body: invalid_body)
          expect(response).to have_http_status(:bad_request)
        end

        it 'does not create a new tweet' do
          expect do
            post api_tweets_path(user_id: user1.id, body: invalid_body)
          end.not_to change(Tweet, :count)
        end
      end

      context 'When the tweet might be a duplicate' do
        let(:valid_body) { 'This is a valid tweet' }

        before do
          create(:tweet, user: user1, body: valid_body)
        end

        it 'returns an error response' do
          post api_tweets_path(user_id: user1.id, body: valid_body)
          expect(response).to have_http_status(:bad_request)
        end

        it 'does not create a new tweet' do
          expect do
            post api_tweets_path(user_id: user1.id, body: valid_body)
          end.not_to change(Tweet, :count)
        end
      end
    end
  end
end
