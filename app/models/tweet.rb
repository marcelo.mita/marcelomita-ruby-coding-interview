class Tweet < ApplicationRecord
  MAX_SIZE_BODY = 180
  belongs_to :user
  validates :body, length: {maximum: MAX_SIZE_BODY }
end
